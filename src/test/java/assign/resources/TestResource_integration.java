package assign.resources;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import assign.services.DBService;
import assign.services.Meeting;
import assign.services.Project;

public class TestResource_integration {
	  Client client;
	  Response response;
	  Random randomGenerator = new Random();
	  ArrayList<Integer> randomList = new ArrayList<Integer>();
	  DBService test = new DBService();
	  
	  @Before
	  public void setUp() throws Exception {
	    client = ClientBuilder.newClient();

	  }
	  
	  
	  @Test
	  public void test_post_project_1() throws Exception {
		  Form form = new Form();
		  String name = "name-" + randomGenerator.nextInt(1000);
		  String description = "description-" + randomGenerator.nextInt(1000);
		  form.param("name", name);
		  form.param("description", description);
		  String response = client.target("http://localhost:8080/assignment5/myeavesdrop/projects").request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class); 
		  if (response == null)
			  throw new RuntimeException("Failed to create");
		  
		  Project actual = test.getProjectByName(name);
		  assertTrue(actual != null);
		  assertEquals(actual.getName(),name);
		  
		  assertEquals(actual.getDescription(),description);
		  
	  }
	  
	  @Test
	  public void test_post_project_2() throws Exception {
		  Form form = new Form();
		  String name = "name-" + randomGenerator.nextInt(1000);
		  String description = "description-" + randomGenerator.nextInt(1000);
		  form.param("name", name);
		  form.param("description", description);
		  String response = client.target("http://localhost:8080/assignment5/myeavesdrop/projects").request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class); 
		  if (response == null)
			  throw new RuntimeException("Failed to create");

		  Project actual = test.getProjectByName(name);
		  assertTrue(actual != null);
		  assertEquals(actual.getName(),name);
		  
		  assertEquals(actual.getDescription(),description);
		  
	  }
	  
	  @Test
	  public void test_post_project_3() throws Exception {
		  Form form = new Form();
		  String name = "name-" + randomGenerator.nextInt(1000);
		  String description = "description-" + randomGenerator.nextInt(1000);
		  form.param("name", name);
		  form.param("description", description);
		  String response = client.target("http://localhost:8080/assignment5/myeavesdrop/projects").request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class); 
		  if (response == null)
			  throw new RuntimeException("Failed to create");

		  Project actual = test.getProjectByName(name);
		  assertTrue(actual != null);
		  assertEquals(actual.getName(),name);
		  
		  assertEquals(actual.getDescription(),description);
		  
	  }
	  
	  
	  @Test
	  public void test_post_project_4() throws Exception {
		  Form form = new Form();
		  String name = "name1";
		  String description = "description-" + randomGenerator.nextInt(1000);
		  form.param("name", name);
		  form.param("description", description);
		  String response = client.target("http://localhost:8080/assignment5/myeavesdrop/projects").request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class); 
		  if (response == null)
			  throw new RuntimeException("Failed to create");

		  Project actual = test.getProjectByName(name);
		  assertTrue(actual != null);
		  assertEquals(actual.getName(),name);
		  
		  assertEquals(actual.getDescription(),description);
		  
	  }
	  
	  @Test
	  public void test_update_project_5() throws Exception{
		  Form form = new Form();
		  form.param("description", "New Description");
		  String response = client.target("http://localhost:8080/assignment5/myeavesdrop/projects/barbican").request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class); 
		  if (response == null)
			  throw new RuntimeException("Failed to create");
		  Project actual = test.getProjectByName("barbican");
		  assertTrue(actual != null);	  
		  assertEquals(actual.getDescription(),"New Description");
	  }
	  
	  
	  @Test
	  public void test_update_project_6() throws Exception{
		  Form form = new Form();
		  form.param("description", "New Description");
		  String response = client.target("http://localhost:8080/assignment5/myeavesdrop/projects/name1").request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class); 
		  if (response == null)
			  throw new RuntimeException("Failed to create");
		  Project actual = test.getProjectByName("barbican");
		  assertTrue(actual != null);	  
		  assertEquals(actual.getDescription(),"New Description");
	  }
	  
	  
	  
	  @Test
	  public void test_post_meeting_for_project_7() throws Exception {
		  Form form = new Form();
		  
		  String name = "name-" + randomGenerator.nextInt(1000);
		  String link = "link-" + randomGenerator.nextInt(1000);
		  String year = ""+randomGenerator.nextInt(1000);
		  
		  form.param("name", name);
		  form.param("link", link);
		  form.param("year", year );
		  String response = client.target("http://localhost:8080/assignment5/myeavesdrop/projects/barbican/meetings").request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class); 
		  if (response == null)
			  throw new RuntimeException("Failed to create");
		  
		  Meeting actual = test.getMeetingByName(name);
		  assertTrue(actual != null);
		  assertEquals(actual.getName(),name);
		  assertEquals(actual.getLink(), link);
		  assertEquals(actual.getYear(), year);

		  
	  }
	  
	  @Test
	  public void test_post_meeting_for_project_8() throws Exception {
		  Form form = new Form();
		  String name = "name-" + randomGenerator.nextInt(1000);
		  String link = "link-" + randomGenerator.nextInt(1000);
		  String year = ""+randomGenerator.nextInt(1000);
		  
		  form.param("name", name);
		  form.param("link", link);
		  form.param("year", year );
		  String response = client.target("http://localhost:8080/assignment5/myeavesdrop/projects/cs340/meetings").request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class); 
		  if (response == null)
			  throw new RuntimeException("Failed to create");
		  
		  
		  Meeting actual = test.getMeetingByName(name);
		  assertTrue(actual != null);
		  assertEquals(actual.getName(),name);
		  assertEquals(actual.getLink(), link);
		  assertEquals(actual.getYear(), year);

	  }
	  
	  @Test
	  public void test_post_meeting_for_project_9() throws Exception {
		  Form form = new Form();
		  
		  String name = "name-" + randomGenerator.nextInt(1000);
		  String link = "link-" + randomGenerator.nextInt(1000);
		  String year = ""+randomGenerator.nextInt(1000);
		  
		  form.param("name", name);
		  form.param("link", link);
		  form.param("year", year );
		  String response = client.target("http://localhost:8080/assignment5/myeavesdrop/projects/cs347/meetings").request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class); 
		  if (response == null)
			  throw new RuntimeException("Failed to create");
		  
		  Meeting actual = test.getMeetingByName(name);
		  assertTrue(actual != null);
		  assertEquals(actual.getName(),name);
		  assertEquals(actual.getLink(), link);
		  assertEquals(actual.getYear(), year);
		  
	  }
	  
	  
	  @Test
	  public void test_get_project_by_name_10() throws Exception {

		  
		  String response =client.target("http://localhost:8080/assignment5/myeavesdrop/projects/cs378").request().get(String.class); 
		  if (response == null)
			  throw new RuntimeException("Failed to create");
		  
		  
		  String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><project><name>cs378</name><description>Project representing</description><meetings><link>m1_link</link><name>m1</name><year>2014</year></meetings></project>";
		  assertEquals(expected, response);
	  }
	  
	  @Test
	  public void test_delete_project_by_name_11() throws Exception {

		  
		  client.target("http://localhost:8080/assignment5/myeavesdrop/projects/name1").request().delete(String.class); 
		 		  
	  }
	  

	  
	  
	  

}
