package assign.services;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;


@XmlRootElement(name = "project")
@XmlType(propOrder={"name", "description", "meetings"} )
public class Project {
	

    private String description;
	
    
    private String name;
    

    
//    @XmlList
    private List<Meeting> meetings = new ArrayList<Meeting>();

    public Project(){
    	description = null;
    	name = null;
    }
    
    public Project(String name, String description)
    {
        this.name = name;
        this.description = description;
        
        this.meetings = new ArrayList<Meeting>();
    }
    
    public void add_meeting(Meeting m){
    	this.meetings.add(m);
    }
    
    public void setName(String name){
    	this.name = name;
    }
    
    @XmlElement
    public String getName(){
    	return this.name;
    }
    
    public void setDescription(String description){
    	this.description = description;
    }
    
    @XmlElement
    public String getDescription(){
    	return this.description;
    }
    
    
    public void setMeetings(List<Meeting> meetings){
    	this.meetings = meetings;
    }
    
    @XmlElement
    public List<Meeting> getMeetings(){
    	return this.meetings;
    }
    
    public boolean equals(Project other)
    {
       if (this.name.equals(other.name) )
       {
          return true;
       }
       return false;

    }
}
