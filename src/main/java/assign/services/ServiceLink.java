package assign.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ServiceLink {

	public String getURL(String[] link){
		String type = link[0];
		String project = link[1];
		String year = link[2];
		String contextPath= "http://eavesdrop.openstack.org/";
		
//		if(type.equals("irclogs") && !project.isEmpty())
//			project = "%23" + project;
		String wholePath = contextPath + type + "/"+ project +"/" + year;
		return wholePath;
	}
	
	public List<String> getData(String[] link, boolean isLink){
		
		
		String url =this.getURL(link);
		Document doc =null;
		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			
		return this.getQueryData(doc, isLink);
	}
	
	public List<String> getQueryData(Document doc, boolean isLink){
		Elements links = doc.select("a[href]");
		List<String> all = new ArrayList<String>();
		int count =0;
		for (Element s_links : links) {
			String x;
			if(isLink){
				x = s_links.absUrl("href");
			}
			else{
				x = s_links.text();
			}
			
			if(count > 4 ){
//				String a = "<a href="+ x +">" + x +"</a>";
				all.add(x);
			}
			++count;
		}
		return all;
	}
	

}
