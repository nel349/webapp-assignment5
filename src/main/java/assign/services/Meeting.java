package assign.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

//@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Meeting")
@XmlAccessorType
public class Meeting {


	private String name;

	private String link;

	private String year;

	public Meeting(){
		name = null;
		link = null;
		year = null;
	}
	
	public Meeting(String name, String link, String year){
		this.name = name;
		this.link = link;
		this.year = year;
	}
	
	
	public void setName(String name){
		this.name = name;
	}
	@XmlElement
	public String getName(){
		return this.name;
	}
	
	public void setLink(String link){
		this.link = link;
	}
	
	@XmlElement
	public String getLink(){
		return this.link;
	}
	
	public void setYear(String year){
		this.year = year;
	}
	
	@XmlElement
	public String getYear(){
		return this.year;
	}


}
