package assign.services;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.sql.*;

import org.apache.commons.dbcp2.BasicDataSource;
import org.json.simple.JSONObject;


public class DBService {

	Logger infoLogger = Logger.getLogger("assignment5");
	String dbURL = "";
	DataSource ds;

	public DBService() {
		dbURL = "jdbc:mysql://localhost:3306/project5";
		ds = setupDataSource(dbURL);

	}

	public static DataSource setupDataSource(String connectURI) {
		BasicDataSource ds = new BasicDataSource();
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUsername("nel349");
		ds.setPassword("");
		ds.setUrl(connectURI);
		return ds;
	}


	private Connection getConnection() {
		Connection conn = null;		
		try {
			conn = DriverManager.getConnection(dbURL,"devdatta", "");

			DatabaseMetaData meta = (DatabaseMetaData) conn.getMetaData();

			if (meta.supportsResultSetType(ResultSet.TYPE_FORWARD_ONLY)) {
				System.out.println("type name=TYPE_FORWARD_ONLY");
			}
			if (meta.supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE)) {
				System.out.println("type name=TYPE_SCROLL_INSENSITIVE");
			}
			if (meta.supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE)) {
				System.out.println("type name=TYPE_SCROLL_SENSITIVE");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}





	public void post_project(String name, String description) throws SQLException{
		Project project = new Project();
		project.setName(name);
		project.setDescription(description);

		String query = "INSERT INTO  projects VALUES(?,?)";
		Map<String, Object> projectInfo = new HashMap<String, Object>();
		projectInfo.put("name",name);
		projectInfo.put("description",description);

		PreparedStatement ps = prepareQuery(query, projectInfo);

		executeUpdate(ps);
	}

	public void put_project(String name, String description) throws SQLException{
		Project project = new Project();
		project.setName(name);
		project.setDescription(description);

		String query = "update projects set description='"+ description+"' where name=?";
		PreparedStatement ps = prepareQuery(query, name);
		executeUpdate(ps);
	}


	public void post_project_meeting(String name, String link, String year, String project_name) throws SQLException{


		String query = "insert into meetings values(?,?,?,?)";
		Map<String, Object> projectInfo = new HashMap<String, Object>();
		projectInfo.put("name",name);
		projectInfo.put("link",link);
		projectInfo.put("year", year);
		projectInfo.put("project_name", project_name);

		PreparedStatement ps = prepareQuery_meeting_for_project(query, projectInfo);

		executeUpdate(ps);
	}


	public Project getProjectByName(String project_name) throws Exception {
		//		String query = "select projects.name, description, meetings.name as meeting_name, link, year, project_name from projects inner join meetings on projects.name = meetings.project_name and projects.name =?";
		String query = "select *from projects where name = ?";

		PreparedStatement ps = prepareQuery(query, project_name);
		ResultSet results = executeQuery(ps);	
		Project project = new Project();
		try {
			if (!results.next()) {
				return null;
			}			

			project.setName(results.getString("name"));
			project.setDescription(results.getString("description"));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		query = "select *from meetings where project_name =?";
		ps = prepareQuery(query, project_name);
		results = executeQuery(ps);
		
		try{
			Meeting m = null;
			while(results.next()){
				m = new Meeting();
				String name = results.getString("name");
				String link = results.getString("link");
				String year = results.getString("year");
				
				m.setLink(link);
				m.setName(name);
				m.setYear(year);
				infoLogger.info(name);
				infoLogger.info(link);
				infoLogger.info(year);
				project.add_meeting(m);
			}


			
		}
		catch(SQLException e){
			e.printStackTrace();
		}

		return project;
	}
	
	
	public Meeting getMeetingByName(String name){
		String query = "select *from meetings where name = ?";

		PreparedStatement ps = prepareQuery(query, name);
		ResultSet results = executeQuery(ps);	
		Meeting meeting = new Meeting();
		try {
			if (!results.next()) {
				return null;
			}			

			meeting.setName(results.getString("name"));
			meeting.setLink(results.getString("link"));
			meeting.setYear(results.getString("year"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return meeting;

	}



	public void deleteProjectByName(String project_name) throws Exception {
		String query = "delete from projects where name=?";
		PreparedStatement ps = prepareQuery(query, project_name);
		executeUpdate(ps);

		query = "delete from meetings where project_name=?";
		ps = prepareQuery(query, project_name);
		executeUpdate(ps);
	}

	public String convertToXML(ResultSet resultSet)
			throws Exception {
		StringBuffer xmlArray = new StringBuffer("<results>");
		while (resultSet.next()) {
			int total_rows = resultSet.getMetaData().getColumnCount();
			xmlArray.append("<result ");
			for (int i = 0; i < total_rows; i++) {
				xmlArray.append(" "+resultSet.getMetaData().getColumnLabel(i + 1) .toLowerCase()+"='"+resultSet.getObject(i + 1)+"'"); }
			xmlArray.append(" />");
		}
		xmlArray.append("</results>");
		return xmlArray.toString();
	}




	public String getallProjects() throws Exception {
		String query = "select * from projects";		
		Connection conn = ds.getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet results = executeQuery(ps);		
		String result = "";
		try {
			if (!results.next()) {
				return null;
			}			

			result += results.getString("name");
			result += " "+ results.getString("description");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}





//	@SuppressWarnings("unchecked")
//	private List<String> compileResults_meetings(ResultSet r) {
//		List<String> list = new ArrayList<String>();
//		try {
//			JSONObject obj = new JSONObject();
//			while(r.next()) {
//
//
//				obj.put("name", r.getString("name"));
//				obj.put("description", r.getString("description"));
//				obj.put("meeting_name", r.getString("meeting_name"));
//				obj.put("link", r.getString("link"));
//				obj.put("year", r.getString("year"));
//				obj.put("project_name", r.getString("project_name"));
//			}
//			list.add(obj.toJSONString());
//
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return list;
//	}

//	private Project compileResults(ResultSet r) {
//		Project project = null;		
//		try {
//			if (!r.next()) {
//				return null;
//			}			
//			project = new Project();
//			project.setName(r.getString("name"));
//			project.setDescription(r.getString("description"));
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return project;
//	}


	private void executeUpdate(PreparedStatement ps) {
		try {
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private ResultSet executeQuery(PreparedStatement ps) {
		ResultSet results = null;
		try {
			results = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return results;
	}


	private PreparedStatement prepareQuery(String query, String value) {
		PreparedStatement statement = null;
		try {
			Connection conn = ds.getConnection();
			statement = conn.prepareStatement(query);
			statement.setString(1, value);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return statement;
	}

	private PreparedStatement prepareQuery(String query, Map<String,Object>projectInfo) {
		PreparedStatement statement = null;
		try {
			Connection conn = ds.getConnection();
			statement = conn.prepareStatement(query);
			statement.setString(1, (String)projectInfo.get("name"));			 
			statement.setString(2, (String)projectInfo.get("description"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return statement;
	}

	private PreparedStatement prepareQuery_meeting_for_project(String query, Map<String,Object>projectInfo) {
		PreparedStatement statement = null;
		try {
			Connection conn = ds.getConnection();
			statement = conn.prepareStatement(query);
			statement.setString(1, (String)projectInfo.get("name"));			 
			statement.setString(2, (String)projectInfo.get("link"));
			statement.setString(3, (String)projectInfo.get("year"));
			statement.setString(4, (String)projectInfo.get("project_name"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return statement;
	}
}
