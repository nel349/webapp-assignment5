package assign.resources;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAttribute;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import assign.services.DBService;
import assign.services.Link;
import assign.services.Project;
import assign.services.ServiceLink;



@Path("/myeavesdrop")
public class RESTEasyHelloWorldService {

	DBService ps = new DBService();
	@POST
	@Path("/projects")
	public String post_project(@FormParam("name") String name, @FormParam("description") String description) throws SQLException {	
		ps.post_project(name, description);
		
		return name + " " + description;
	}

	
	@POST
	@Path("/projects/{name}")
	public String put_project(@PathParam("name")String name,@FormParam("description") String description) throws SQLException { 
		ps.put_project(name, description);
//		System.out.println("EJECUTADO");
		return name + " EJECUTADO" + description;
	}
	
	@POST
	@Path("/projects/{project_name}/meetings")
	public String post_meeting_on_project(@FormParam("name")String name, @FormParam("link") String link, @FormParam("year") String year, @PathParam("project_name") String project_name) throws SQLException { 
		ps.post_project_meeting(name, link, year, project_name);
//		System.out.println("EJECUTADO");
		return name + " EJECUTADO" + name  +"  " + year + " " + project_name;
	}
	
	
	@GET
	@Path("/projects/{project_name}")
	@Produces("application/xml")
	public Project get_project(@PathParam("project_name") String project_name) throws Exception{
		Project result = ps.getProjectByName(project_name);
		return result;
	}
	
	
	@DELETE
	@Path("/projects/{project_name}")
	public void delete_project(@PathParam("project_name") String project_name) throws Exception{
		ps.deleteProjectByName(project_name);
		
	}
}