CREATE DATABASE  IF NOT EXISTS `project5`;

USE project5;
SET SQL_SAFE_UPDATES = 0;

DROP TABLE IF EXISTS projects CASCADE;

-- "CASCADE" means drop any constraints that other
-- tables may have on this table when it is deleted 
CREATE TABLE projects (
  name     VARCHAR(30) NOT NULL PRIMARY KEY,
  description VARCHAR(30)
  );

DROP TABLE IF EXISTS meetings CASCADE;
CREATE TABLE meetings (
  name    VARCHAR(30) NOT NULL PRIMARY KEY, 
  link   VARCHAR(30),
  year   int(4),
  project_name  VARCHAR(30)
  );


INSERT INTO  projects VALUES("cs378",'Project representing');
			

INSERT INTO meetings VALUES("m1", "m1_link", 2014, "cs378");


INSERT INTO  projects VALUES("barbican",'proeyctproyecto');
			

INSERT INTO meetings VALUES("junta", "http://www.linkvale.com", 2013, "barbican");


INSERT INTO  projects VALUES("borrar1",'proeyctproyecto');

INSERT INTO  projects VALUES("borrar2",'proeyctproyecto');

INSERT INTO  projects VALUES("borrar3",'proeyctproyecto');







